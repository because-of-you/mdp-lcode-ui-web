/* Layout */
import { Layout } from '@/utils/routerHelper'
const { t } = useI18n()

const RoutesLcode: AppRouteRecordRaw[] = [ 
  {
    path: '/mdp/lcode/gen',
    component: Layout,
    name: 'lcodeGen', 
    meta: {
      title: t('router.lcode.gen'),
      icon: 'simple-line-icons:screen-tablet'
    },
    //leaf: true,//只有一个节点
    children: [
      {
        path: 'index',
        component: () => import('@/views/mdp/lcode/gen/Index.vue'),
        name: 'lcodeGenIndex',
        meta: { title: t('router.lcode.CodeGenIndex'), icon: 'simple-line-icons:organization',noCache:false,
        menu: true },
        keepAlive: true
      }
    ]
  }
]

export default RoutesLcode
