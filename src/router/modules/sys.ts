/* Layout */
import { Layout } from '@/utils/routerHelper'
const { t } = useI18n()

const RoutesSys: AppRouteRecordRaw[] = [
  {
    path: '/mdp',
    component: Layout,
    meta: {
      title: t('router.sys.RoleQxMng'),
      icon: 'simple-line-icons:people'
    },
    name: 'sysRoleQx',  
    children: [
      {
        path: 'sys/role/index',
        component: () => import('@/views/mdp/sys/role/Index.vue'),
        name: 'sysRoleIndex',
        meta: { title: t('router.sys.RoleMng'), menu: true }
      },
      {
        path: 'sys/qx/index',
        component: () => import('@/views/mdp/sys/qx/Index.vue'),
        name: 'sysQxIndex',
        meta: { title: t('router.sys.QxMng'), menu: true }
      },
      {
        path: 'menu/index',
        component: () => import('@/views/mdp/menu/menuDef/Index.vue'),
        name: 'sysMenuIndex',
        meta: { title: t('router.sys.MenuDefMng'), menu: true }
      },
      {
        path: 'menu/module/index',
        component: () => import('@/views/mdp/menu/menuModule/Index.vue'),
        name: 'sysMenuModuleIndex',
        meta: { title: t('router.sys.MenuModuleMng'), menu: true }
      },
      {
        path: 'menu/module/branch',
        component: () => import('@/views/mdp/menu/menuModuleBranch/Index.vue'),
        name: 'sysMenuModuleBranch',
        meta: { title: t('router.sys.MenuModuleBranchMng'), menu: true }, 
      }
    ]
  },
  {
    path: '/mdp/sys',
    component: Layout,
    meta: {
      title: t('router.sys.OrgMng'),
      icon: 'simple-line-icons:grid'
    },
    name: 'sysOrgMng',  
    children: [
      {
        path: 'branch/set',
        component: () => import('@/views/mdp/sys/branch/BranchSet.vue'),
        name: 'sysBranchSet',
        meta: { title: t('router.sys.BranchSet'), menu: true }
      },
      {
        path: 'user/index',
        component: () => import('@/views/mdp/sys/user/Index.vue'),
        name: 'sysUserIndex',
        meta: { title: t('router.sys.UserMng'), menu: true }
      },

      {
        path: 'dept/index',
        component: () => import('@/views/mdp/sys/dept/Index.vue'),
        name: 'sysDeptIndex',
        meta: { title: t('router.sys.DeptMng'), menu: true }
      },
      {
        path: 'branch/index',
        component: () => import('@/views/mdp/sys/branch/Index.vue'),
        name: 'sysBranchIndex',
        meta: { title: t('router.sys.BranchMng'), menu: true }
      },
      {
        path: 'post/index',
        component: () => import('@/views/mdp/sys/post/Index.vue'),
        name: 'sysPostIndex',
        meta: { title: t('router.sys.PostMng'), menu: true }
      },
      {
        path: 'dept/post/role/index',
        component: () => import('@/views/mdp/sys/deptPost/DeptPostRole.vue'),
        name: 'sysDeptPostRoleIndex',
        meta: { title: t('router.sys.DeptPostRole'), menu: true }
      },
      {
        path: 'user/adm',
        component: () => import('@/views/mdp/sys/user/BranchAdm.vue'),
        name: 'sysBranchUserAdm',
        meta: { title: t('router.sys.BranchAdm'), menu: true }
      },
      {
        path: 'user/join/require/adm',
        component: () => import('@/views/mdp/sys/userJoinRequire/Index.vue'),
        name: 'sysUserJoinApprove',
        meta: { title: t('router.sys.UserJoinApprove'), menu: true }
      },
      {
        path: 'user/unregister',
        component: () => import('@/views/mdp/sys/user/UserUnregister.vue'),
        name: 'sysUserUnregister',
        meta: { title: t('router.sys.UserUnregister'), menu: true }
      },
      {
        path: 'branch/maxUsersSet',
        component: () => import('@/views/mdp/sys/branch/MaxUsersSet.vue'),
        name: 'sysMaxUsersSet',
        meta: { title: t('router.sys.MaxUsersSet'), menu: true }
      },
    ]
  },
  {
    path: '/mdp/plat',
    component: Layout,
    meta: {
      title: t('router.sys.plat'),
      icon: 'simple-line-icons:globe-alt'
    },
    name: 'sysPlat', 
    children: [
      {
        path: 'platform/PlatformMng',
        component: () => import('@/views/mdp/plat/platform/PlatformMng.vue'),
        name: 'sysPlatformMng',
        meta: { title: t('router.sys.PlatformMng'), menu: true }
      },
      {
        path: 'userValidInfo',
        component: () => import('@/views/mdp/sys/userValidInfo/UserValidInfoMngForPerson.vue'),
        name: 'sysUserValidInfo',
        meta: { title: t('router.sys.UserValidInfoMngForPerson'), menu: true }
      },
      {
        path: 'branchValidInfo',
        component: () => import('@/views/mdp/sys/userValidInfo/UserValidInfoMngForBranch.vue'),
        name: 'sysBranchValidInfo',
        meta: { title: t('router.sys.UserValidInfoMngForBranch'), menu: true }
      }
    ]
  },
  {
    path: '/mdp/meta',
    component: Layout,
    meta: {
      title: t('router.sys.MetaData'),
      icon: 'simple-line-icons:share-alt'
    },
    name: 'sysMetaData',
    //leaf: true,//只有一个节点
    //参数路径-------------------------------------------------------
    children: [
      {
        path: 'item/index',
        component: () => import('@/views/mdp/meta/item/Index.vue'),
        name: 'sysItemIndex',
        meta: { title: t('router.sys.ItemMng'), menu: true }
      },
      {
        path: 'item/option/index',
        component: () => import('@/views/mdp/meta/itemOption/Index.vue'),
        name: 'sysItemOptionIndex',
        meta: { title: t('router.sys.ItemList'), menu: true }, 
      },
      {
        name: 'sysItemSet',
        path: 'item/set/:itemCode',
        component: () => import('@/views/mdp/meta/item/Set.vue'),
        meta: { title: '参数改值', hidden: true }
      },
      //如 http://wxxx.com/sys/m1/mdp/meta/item/set/xxxx?categoryId=all ,其中 categoryId 取值all、sysParams
      {
        path: 'params/index',
        component: () => import('@/views/mdp/meta/sysParam/Index.vue'),
        name: 'sysParamsIndex',
        meta: { title: t('router.sys.SysParamMng'), menu: true }
      }
    ]
  },
  {
    path: '/mdp/tpa',
    component: Layout,
    meta: {
      title: t('router.sys.tpaMng'),
      icon: 'fa:wechat'
    },
    name: 'sysTpa',
    //leaf: true,//只有一个节点
    children: [
      {
        path: 'invite/index',
        component: () => import('@/views/mdp/sys/userTpaInvite/Index.vue'),
        name: 'sysInviteIndex',
        meta: { title: t('router.sys.userTpaInviteIndex'), menu: true }
      },

      {
        path: 'user/index/:inviteId',
        component: () => import('@/views/mdp/sys/userTpa/Index.vue'),
        name: 'sysUserTpaInviteIndex',
        meta: { title: t('router.sys.myInviteUserTpa'), menu: true }
      },
      {
        path: 'user/index',
        component: () => import('@/views/mdp/sys/userTpa/Index.vue'),
        name: 'sysUserTpaIndex',
        meta: { title: t('router.sys.userTpaIndex'), menu: true }
      }
    ] 
  }
]

export default RoutesSys
