import axios from '@/config/maxios'

 import config from '@/api/mdp_pub/mdp_config'

let base = config.getDmCtx();
 
//查询数据源列表
export const dataSourceList = params => { return axios.get(`${base}/mdp/dm/meta/dataSource/list`, {params:params}); };


//查询表格列表
export const getTableColumnList = params => { return axios.get(`${base}/mdp/dm/meta/table/column/list`, {params:params}); };

export const getSqlColumnList = params => { return axios.get(`${base}/mdp/dm/meta/sql/column/list`, {params:params}); };


//查询表格列表
export const getTableList = params => { return axios.get(`${base}/mdp/dm/meta/table/list`, {params:params}); };

//查询表格列表
export const delTable = params => { return axios.post(`${base}/mdp/dm/meta/table/del`, params); };

//查询表格列表
export const editSomeFieldsTable = params => { return axios.post(`${base}/mdp/dm/meta/table/editSomeFields`, params); };

//新增表格列
export const addTableColumn = params => { return axios.post(`${base}/mdp/dm/meta/table/column/add`, params); };

//删除表格列
export const delTableColumn = params => { return axios.post(`${base}/mdp/dm/meta/table/column/del`, params); };

//批量删除表格列
export const batchDelTableColumn = params => { return axios.post(`${base}/mdp/dm/meta/table/column/batchDel`, params); };

//批量修改表格列
export const editSomeFieldsTableColumn = params => { return axios.post(`${base}/mdp/dm/meta/table/column/editSomeFields`, params); };

