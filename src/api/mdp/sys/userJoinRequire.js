import axios from '@/config/maxios'

import config from '@/api/mdp_pub/mdp_config'

let base = config.getSysCtx();

/**
 * 企业入驻审核流程
 * 1 默认只开放普通查询，所有查询，只要上传	 分页参数 {pageNum:当前页码从1开始,pageSize:每页记录数,total:总记录【数如果是0后台会自动计算总记录数非0不会自动计算】}，后台都会自动按分页查询 其它 api用到再打开，没用到的api请注释掉，
 * 2 查询、新增、修改的参数格式  params={id:'主键 主键',agree:'审批状态同意1不同意0',joinUserid:'用户编号',joinUsername:'用户名称',joinBranchId:'要加入的机构号',joinDeptid:'要加入的部门号',createDate:'申请加入时间',agreeDate:'同意时间',joinStatus:'状态0初始1同意2拒绝',joinReason:'加入理由',bizProcInstId:'当前流程实例编号',bizFlowState:'当前流程状态',joinUserPhoneno:'联系电话'}
 * @author maimeng-mdp code-gen
 * @since 2024-5-13
 **/
 
//普通查询 条件之间and关系  
export const listUserJoinRequire = params => { return axios.get(`${base}/mdp/sys/userJoinRequire/list`, { params: params }); };

//普通查询 条件之间and关系
export const queryUserJoinRequireById = params => { return axios.get(`${base}/mdp/sys/userJoinRequire/queryById`, { params: params }); };

//删除一条企业入驻审核流程 params={id:'主键 主键'}
export const delUserJoinRequire = params => { return axios.post(`${base}/mdp/sys/userJoinRequire/del`,params); };

//批量删除企业入驻审核流程  params=[{id:'主键 主键'}]
export const batchAddUserJoinRequire = params => { return axios.post(`${base}/mdp/sys/userJoinRequire/batchAdd`, params); };

//批量删除企业入驻审核流程  params=[{id:'主键 主键'}]
export const batchDelUserJoinRequire = params => { return axios.post(`${base}/mdp/sys/userJoinRequire/batchDel`, params); };

//修改一条企业入驻审核流程记录
export const editUserJoinRequire = params => { return axios.post(`${base}/mdp/sys/userJoinRequire/edit`, params); };

//新增一条企业入驻审核流程
export const addUserJoinRequire = params => { return axios.post(`${base}/mdp/sys/userJoinRequire/add`, params); };

//批量修改某些字段
export const editSomeFieldsUserJoinRequire = params => { return axios.post(`${base}/mdp/sys/userJoinRequire/editSomeFields`, params); };