import axios from '@/config/maxios'
import config from '@/api/mdp_pub/mdp_config'

let base = config.getSysCtx()


export const getAllMenuModule = params => { return axios.get(`${base}/mdp/menu/menuModule/list`, {params: params }); };
export const getBuyMenuModule = params => { return axios.get(`${base}/mdp/menu/menuModuleBranch/list`, {params: params }); };
